#include <stdio.h>

// Prototipo de función
int sumaDigitos(int nPos);

int main(void){
	// Declarar variable n.
	unsigned int n;
	// Declarar variable num.
	unsigned int num;
	// Declarar variable s.
	unsigned int s;

	// Preguntar a usuario por número entero positivo.
	printf("Ingresa número entero positivo: ");
	// Obtener número entero positivo y asignarlo a variable n.
	scanf("%u", &n);

	// Asignar valor de n a num (almacenador temporal para obtener dígitos).
	num = n;

	// Hacer:
	do{
		num = s = sumaDigitos(num);
		// Mientras s sea mayor a 9.
	}while (s > 9);

	// Si s es igual a 9:
	if (s == 9){
		// Imprimir: número es divisible por 9.
		printf("%u es divisible por 9.\n", n);
	}
	// De lo contrario:
	else {
		// Imprimir: número no es divisible por 9.
		printf("%u no es divisible por 9.\n", n);
	}
}


//### Funcion: suma de digitos
//#### Entradas
//- Valor entero positivo: `ePos`
//#### Salidas
//- Total de la suma de dígitos: `suma`.
int sumaDigitos(int ePos){

//1. Asignar `suma` (suma de dígitos) el valor de 0.
	int suma = 0;
//1. Mientras `ePos` sea mayor a `0`:
	while (ePos > 0){
//   1. Sumar a `suma` el módulo  `10` de `ePos`.
		suma += ePos % 10;
//   1. Imprimir  "+ " `ePos % 10`.
		printf("+%d\n", ePos % 10);
//   1. Dividir `ePos` en `10` y asignar resultado a `ePos`.
		ePos /= 10;
	}
//1. Imprimir valor de `suma`.
	printf("Total: %d\n", suma);

	return suma;
}
