# Función

Sea n = a<sub>k</sub> a<sub>k-1</sub> a<sub>k-2</sub> ... a<sub>1</sub>
a<sub>0</sub> un número entero, donde a<sub>k</sub> a<sub>k-1</sub>
a<sub>k-2</sub> ... a<sub>1</sub> a<sub>0</sub> representan cada uno de los
dígitos del número n. Sea s = a<sub>k</sub> + a<sub>k-1</sub> + a<sub>k-2</sub>
\+ ... + a<sub>1</sub> + a<sub>0</sub> la suma de los dígitos de n. Se sabe que
n es divisible entre 9 si y sólo si al realizar sumas sucesivas de los dígitos
de s hasta obtener un solo dígito, éste último es igual a 9.

Por ejemplo, suponga que `n = 27193257`. Entonces, el primer 
`s = 2 + 7 + 1 + 9 + 3 + 2 + 5 + 7 = 36`. Como el resultado tiene dos dígitos,
se suman nuevamente los dígitos obteniendo un nuevo `s = 3 + 6 = 9`. Dado que
este resultado tiene un único dígito y es igual a `9`, se puede deducir que
`27193257` es divisible por `9`.

Escriba un programa que le solicite al usuario que ingrese un número entero
positivo y luego use el criterio anterior para determinar si el número es
divisible entre `9`. Adicionalmente, se debe mostrar por pantalla el resultado
de cada iteración.

## Refinamiento 1

1. Obtener número entero positivo de usuario.
1. Hacer:
   1. Calcular e imprimir suma de dígitos.
   1. Mientras suma de díginos sea mayor a 9.
1. Si suma de dígitos es igual a 9:
   1. Imprimir: número es divisible por 9.
1. De lo contrario:
   1. Imprimir: número no es divisible por 9.

## Refinamiento 2

```mermaid
graph TD
    ini("Inicio")-->getNum[/"Obtego: número<br>positivo n"/]
    getNum-->numIni[num = n]
    numIni-->sum0["suma = 0"]
    sum0-->numCond{"num > 0"}
    numCond--"Sí"-->sumDig["s += num % 10"]
    sumDig-->printDig[/"Imprimo: num % 10"/]
    printDig-->numDeg["num /= 10"]
    numDeg-->numCond
    numCond--"No"-->printSum[/"Imprimo: s"/]
    printSum-->swapNum["num = s"]
    swapNum-->condRep{"s > 9"}
    condRep--"Sí"-->sum0
    condRep--"No"-->cond9{"s == 9"}
    cond9--"Sí"-->print9[/"Imprimo: n es<br>múltiplo de 9."/]
    print9-->fin("Fin")
    cond9--"No"-->printNo9[/"Imprimo: n no es<br>múltiplo de 9."/]  
    printNo9-->fin
```

1. Preguntar a usuario por número entero positivo.
1. Obtener número entero positivo y asignarlo a variable `n`.
1. Asignar valor de `n` a `num` (almacenador temporal para obtener dígitos).
1. Hacer:
   1. Invoco función `sumaDigitos` y asigno a `s` valor retornado.
   1. Asignar a `num` valor de `s`.
   1. Mientras `s` sea mayor a 9.
1. Si `s` es igual a 9:
   1. Imprimir: número es divisible por 9.
1. De lo contrario:
   1. Imprimir: número no es divisible por 9.

### Funcion: suma de digitos

#### Entradas

- Valor entero positivo: `ePos`

#### Salidas

- Total de la suma de dígitos: `suma`.

#### Pseudo-código

1. Asignar `suma` (suma de dígitos) el valor de 0.
1. Mientras `ePos` sea mayor a `0`:
   1. Sumar a `suma` el módulo  `10` de `ePos`.
   1. Imprimir  "+ " `ePos % 10`.
   1. Dividir `ePos` en `10` y asignar resultado a `ePos`.
1. Imprimir valor de `suma`.

## Refinamiento 3

1. Declarar variable `n`.
1. Declarar variable `num`.
1. Declarar variable `s`.
1. Preguntar a usuario por número entero positivo.
1. Obtener número entero positivo y asignarlo a variable `n`.
1. Asignar valor de `n` a `num` (almacenador temporal para obtener dígitos).
1. Hacer:
   1. Invoco función `sumaDigitos` y asigno a `s` valor retornado.
   1. Asignar a `num` valor de `s`.
   1. Mientras `s` sea mayor a 9.
1. Si `s` es igual a 9:
   1. Imprimir: número es divisible por 9.
1. De lo contrario:
   1. Imprimir: número no es divisible por 9.

### Funcion: suma de digitos

#### Entradas

- Valor entero positivo: `ePos`

#### Salidas

- Total de la suma de dígitos: `suma`.

#### Pseudo-código

1. Asignar `suma` (suma de dígitos) el valor de 0.
1. Mientras `ePos` sea mayor a `0`:
   1. Sumar a `suma` el módulo  `10` de `ePos`.
   1. Imprimir  "+ " `ePos % 10`.
   1. Dividir `ePos` en `10` y asignar resultado a `ePos`.
1. Imprimir valor de `suma`.
